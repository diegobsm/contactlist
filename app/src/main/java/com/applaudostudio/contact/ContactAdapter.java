package com.applaudostudio.contact;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applaudostudio.contact.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private List<Contact> mData;

    public ContactAdapter(){
        mData = new ArrayList<>();
    }

    public void setData(List<Contact> data){
        mData = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_item,viewGroup,false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder contactViewHolder, int i) {
        contactViewHolder.bind(mData.get(i));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ContactViewHolder extends RecyclerView.ViewHolder {

        TextView txvName;
        TextView txvPhone;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            txvName = itemView.findViewById(R.id.txvName);
            txvPhone  = itemView.findViewById(R.id.txvPhone);
        }

        public void bind (Contact contact) {
            txvName.setText(contact.getName());
            txvPhone.setText(contact.getPhone());
        }
    }
}
