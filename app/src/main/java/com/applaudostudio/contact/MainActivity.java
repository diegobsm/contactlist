package com.applaudostudio.contact;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.applaudostudio.contact.model.Contact;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rcvContact = findViewById(R.id.rcvContact);
        rcvContact.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        ContactAdapter contactAdapter = new ContactAdapter();
        rcvContact.setAdapter(contactAdapter);
        contactAdapter.setData(Contact.getContacts());
    }
}
