package com.applaudostudio.contact.model;

import java.util.ArrayList;
import java.util.List;

public class Contact {

    private String mName;
    private String mPhone;

    public Contact(String mName, String mPhone) {
        this.mName = mName;
        this.mPhone = mPhone;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }


    public static List<Contact> getContacts(){
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Contact 1","78464967"));
        contacts.add(new Contact("Contact 2","78764546"));
        contacts.add(new Contact("Contact 3","78469777"));
        contacts.add(new Contact("Contact 4","78735346"));
        contacts.add(new Contact("Contact 5","67453542"));
        contacts.add(new Contact("Contact 6","74848546"));

        return contacts;
    }
}
